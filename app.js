// Debugger
require('./configs/debugger.config')

// App
const express = require('express')
const app = express()

// Configs
require('./configs/middleware.config')(app)
require('./configs/locals.config')(app)

// Base URLS
app.use('/api/caesar', require('./routes/caesar.routes'))

module.exports = app
