const bodyParser = require('body-parser')
const logger = require('morgan')
const cors = require('cors')

const whitelist = [`${process.env.DOMAIN}`]
const corsOptions = {
  origin: `${process.env.DOMAIN}`,
  methods: "GET,HEAD,PUT,PATCH,POST,DELETE,OPTIONS",
  preflightContinue: true,
  credentials: true,
}

module.exports = (app) => {
  app.use(cors(corsOptions))
  app.use(logger('dev'))
  app.use(bodyParser.json())
  app.use(bodyParser.urlencoded({ extended: false }))
}
